---
title: "Avaliacoes"
date: 2022-10-09T20:02:57-03:00
draft: false
---
Nesta página estão as avaliações e o tempo de espera médio de clinicas e hospitais, a nota 1 é a pior nota e a nota 5 é a melhor nota.

1. Clínica do Jose - Nota 4
2. Clínica do Joao - Nota 3
3. Clinica da Maria - Nota 2
4. Clinica da Ana - Nota 1
5. Hospital do Coração - Nota 3 e tempo de espera médio de 30 minutos
6. Hospital Miocardio - Nota 2 e tempo de espera médio de 40 minutos
7. Hospital Ventrículo - Nota 1 e tempo de espera médio de 50 minutos
8. Hospital Àtrio - Nota 5 e tempo de espera médio de 20 minutos
