---
title: "Privado"
date: 2022-09-28T08:18:22-03:00
draft: false
---

Aqui estão 4 clínicas de cardiologia:

1. Clínica do Jose - Rua perto de casa 500 - (11)1234-5678
2. Clínica do Joao - Rua longe de casa 700 - (11)9876-5432
3. Clinica da Maria - Rua logo ali 100 - (11)1111-2222
4. Clinica da Ana - Avenida junto do rio 300 - (11)3333-4444