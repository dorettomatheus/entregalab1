---
title: "Público"
date: 2022-09-28T08:18:10-03:00
draft: false
---
Aqui estão 4 hospitais públicos referência em cardiologia:

1. Hospital do Coração - Rua no centro 500 - (11)1234-9999
2. Hospital Miocardio - Rua perto da USP 700 - (11)9876-6666
3. Hospital Ventrículo - Rua longe da USP 100 - (11)7777-2222
4. Hospital Àtrio - Avenida longe do rio 300 - (11)8888-4444
